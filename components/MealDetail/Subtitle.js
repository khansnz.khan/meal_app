import React from 'react'
import {View,Text,StyleSheet} from 'react-native'

function Subtitle({children}) {
  return (
    <View style={styles.subtitleContainer}>
    <Text style={styles.subTitle}>{children}</Text>
    </View>
  )
}

export default Subtitle
const styles = StyleSheet.create({
    subtitleContainer:{
        padding:6,
        marginVertical:4,
        marginHorizontal:12,
        borderBottomWidth:2,
        borderBottomColor:'#e2b497'
  },
  subTitle:{
    color:'white',
    fontSize:18,
    fontWeight:'bold',
    textAlign:'center',
    color:'#e2b497'
  },
})