import React from 'react';
import {Pressable, Text, StyleSheet} from 'react-native';

function IconButton({onPress}) {
  return (
    <Pressable onPress={onPress}>
      <Text style={styles.text}>Tap me</Text>
    </Pressable>
  );
}

export default IconButton;
const styles = StyleSheet.create({
  text: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
