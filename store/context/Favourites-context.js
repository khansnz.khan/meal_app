import {createContext, useState} from 'react';

const FavouritesContext = createContext({
  id: [],
  addFovourite: id => {},
  removeFavourite: id => {},
});

function FavouriteContext({childen}) {
  const [favouriteMealsIds, setFavouriteMealsIds] = useState([]);

  function addFovourite(id) {
    setFavouriteMealsIds(currentFavIds => [...currentFavIds, id]);
  }
  function removeFavourite(id) {
    setFavouriteMealsIds(currentFavIds =>
      currentFavIds.filter((mealId) => mealId !== id),
    );
  }

  const value = {
    id: favouriteMealsIds,
    addFovourite: addFovourite,
    removeFavourite: removeFavourite,
  };
  return (
    <FavouritesContext.Provider 
    value={value}
    >
      {childen}
    </FavouritesContext.Provider>
  );
}
export default FavouriteContext;
