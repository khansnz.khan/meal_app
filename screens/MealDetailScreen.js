import {useContext, useLayoutEffect} from 'react';
import {Image, ScrollView ,Text, View,StyleSheet, Button} from 'react-native';
import IconButton from '../components/IconButton';
import List from '../components/MealDetail/List';
import Subtitle from '../components/MealDetail/Subtitle';
import MealDetails from '../components/MealDetails';
import {MEALS} from '../data/dummy-data';
import {useDispatch, useSelector} from 'react-redux'
import { addFavourite,removeFavourite} from '../store/redux/favourites'


function MealDetailScreen({route,navigation}) {
  // const favouriteMealsCtx=useContext(FavouritesContext);

//   const mealIsFavourite = favouriteMealsCtx.ids.includes(mealId)

//   const mealId = route.params.mealId;
//  function headerBUttonPressHandler(){
//   if(mealIsFavourite){
//     favouriteMealsCtx.removeFavourite(mealId)
//   }else{
//     favouriteMealsCtx.addFavourite(mealId)
//   }
//  }
const favouriteMealIds = useSelector((state)=>state.favouriteMeals.ids)
 const dispatch = useDispatch()

 function changeFavouriteStatusHandler(){
  if (mealsIsFavourite){
    dispatch(removeFavourite({id:mealId}))

  }else{
    dispatch(addFavourite({id:mealId}))
  }
 }

  useLayoutEffect(()=>{
    navigation.setOptions({
      headerRight:()=>{
        return <IconButton onPress={headerBUttonPressHandler}/>
      }
    })
  },[navigation,headerBUttonPressHandler])

  const selectedMeal = MEALS.find((meals) => meals.id === mealId);
  return (
    <ScrollView style={styles.rootContainer}>
      <Image style={styles.image} source={{uri: selectedMeal.imageUrl}} />
      <Text style={styles.title}>{selectedMeal.title}</Text>
      <MealDetails
        duration={selectedMeal.duration}
        complexity={selectedMeal.complexity}
        affordability={selectedMeal.affordability}
        textStyle = {styles.detailsText}
      />
      <View style={styles.listOuterContainer}>
      <View style={styles.listContainer}> 
     <Subtitle>ingredients</Subtitle>
     <List data={selectedMeal.ingredients}/>
      <Subtitle>Steps</Subtitle>
      <List data={selectedMeal.steps} />
      </View>
      </View>
    </ScrollView>
  );
}

export default MealDetailScreen;
const styles =StyleSheet.create({
  rootContainer:{
    marginBottom:32
  },
  image:{
    width:'100%',
    height:350
  },
  title:{
    fontWeight:'bold',
    fontSize:24,
    margin:8,
    textAlign:'center',
    color:'white'
  },
  detailsText:{
    color:'white'
  },
  subTitle:{
    color:'white',
    fontSize:18,
    fontWeight:'bold',
    textAlign:'center',
    color:'#e2b497'
  },
  subtitleContainer:{
        padding:6,
        marginVertical:4,
        marginHorizontal:24,
        borderBottomWidth:2,
        borderBottomColor:'#e2b497'
  },
  listContainer:{
    width:'80%'
  },
  listOuterContainer:{
    alignItems:'center'

  }
})
