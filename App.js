import CategoriesScreen from './screens/CategoriesScreen';
import {StyleSheet} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import MealsOverviewScreen from './screens/MealsOverviewScreen';
import MealDetailScreen from './screens/MealDetailScreen';
import { createDrawerNavigator}  from '@react-navigation/drawer'
import FavouritesScreen from './screens/FavouritesScreen';
import 'react-native-gesture-handler'
import { Provider } from 'react-redux';
import { store } from './store/redux/store';

const Stack = createNativeStackNavigator();
const Drawer = createDrawerNavigator();

function DrawerNavigator(){
  return <Drawer.Navigator
  screenOptions={{
    headerStyle: {backgroundColor: '#351401'},
    headerTintColor: 'white',
    sceneContainerStyle: {backgroundColor: '#3f2f25'},
    drawerContentStyle:{backgroundColor:'#3f2f25'},
    drawerInactiveTintColor:"white",
    drawerActiveTintColor:'#351401',
    drawerActiveBackgroundColor:'#e4baa1',

  }}>
    <Drawer.Screen name='Categories' component={CategoriesScreen} 
    options={{
      title:'All Categories'
    }} />
    <Drawer.Screen
    name ='Favourites' component={FavouritesScreen} 
    options={{
      title:'About the Meal'
    }}
    />
  </Drawer.Navigator>
}

const App = () => {
  return (
    <Provider store={store}>   
     <NavigationContainer>
      <Stack.Navigator
       >
        <Stack.Screen
          name="DrawerScreen"
          component={DrawerNavigator}
          options={{
            title: 'All Categories',
            headerShown:false
          }}
        />
        <Stack.Screen
          name="MealsOverview"
          component={MealsOverviewScreen}
        />
        <Stack.Screen 
        name='MealDetail'
        component={MealDetailScreen} 
/>

      </Stack.Navigator>
    </NavigationContainer>
     </Provider>

  );
};

export default App;
const styles = StyleSheet.create({});
